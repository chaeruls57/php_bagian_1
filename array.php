<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Array</h2>
    <?php
        echo "<h3>Soal 1</h3>";
        $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
        print_r($kids);
        echo "<h3>Soal 2</h3>";
        echo "Total Kids : ". count($kids);
        echo "<ol>";
        echo "<li>" . $kids[0] ."</li>";
        echo "<li>" . $kids[1] ."</li>";
        echo "<li>" . $kids[2] ."</li>";
        echo "<li>" . $kids[3] ."</li>";
        echo "<li>" . $kids[4] ."</li>";
        echo "<li>" . $kids[5] ."</li>";
        echo "</ol>";
        echo "<br>";
        $adult = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
        print_r($adult);

        echo "<br><br>";
        echo "Total Adult : ". count($adult);
        echo "<ol>";
        echo "<li>" . $adult[0] ."</li>";
        echo "<li>" . $adult[1] ."</li>";
        echo "<li>" . $adult[2] ."</li>";
        echo "<li>" . $adult[3] ."</li>";
        echo "<li>" . $adult[4] ."</li>";
        echo "</ol>";
        
        echo "Soal 3";
        $biodata = [
            ["nama" => "Will Byers", "umur" => 12, "Aliases" => "Will the Wise", "Status" => "Alive"],
            ["nama" => "Mike Wheeler", "umur" => 12, "Aliases" => "Dungeon Master", "Status" => "Alive"],
            ["nama" => "Jim Hopper", "umur" => 12, "Aliases" => "Chief Hopper", "Deceased" => "Alive"],
            ["nama" => "Eleven", "umur" => 12, "Aliases" => "El", "Status" => "Alive"],
        ];

        echo "<pre>";
        print_r($biodata);
        echo "</pre>";
        
    ?>
</body>
</html>