<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>String Php</title>
</head>
<body>
    <h2>Latihan Php</h2>
    <?php
        echo "<h3>Soal 1</h3>";
        $kalimat1 = "PHP is never old";
        echo "Kalimat 1 : " . $kalimat1 . "<br>";
        echo "Panjang String : " . strlen($kalimat1) . "<br>";
        echo "Jumlah Kata : " . str_word_count($kalimat1) . "<br>";

        echo "<h3>Soal 2</h3>";
        $kalimat2 = "Hello World";
        echo "kalimat  : " . $kalimat2 . "<br>";
        echo "kata 1 : ". substr($kalimat2,0,5)."<br>";
        echo "Kata 2 : ". substr($kalimat2,6,5)."<br>";

        echo "<h3>Soal 3</h3>";
        $kalimat3 = "Selamat pagi";
        echo "Kalimat 3  : " . $kalimat3 . "<br>";
        echo "Kalimat 3 di ubah : ". str_replace("pagi","siang",$kalimat3);
        
    ?>
</body>
</html>